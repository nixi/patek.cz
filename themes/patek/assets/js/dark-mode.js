function restoreDarkmode() {
	const elem = document.getElementById("content");
	if (elem && document.cookie.includes("dark-mode=true")) {
		elem.classList.add("dark-mode");
	}
}

function toggleDarkmode() {
	const elem = document.getElementById("content");
	if (elem) {
		const state = elem.classList.toggle("dark-mode");
		if (state) {
			// 31536000 = 1 year; not setting max-age would make it session bound
			document.cookie = "dark-mode=true; path=/; max-age=31536000; secure; samesite=strict";
		} else {
			// Delete the cookie by immediately expiring it
			document.cookie = "dark-mode=; path=/; max-age=0; secure; samesite=strict";
		}
	}
}

document.addEventListener("DOMContentLoaded", restoreDarkmode);
document.addEventListener("DOMContentLoaded", () => {
	for (const toggler of document.getElementsByClassName("dark-mode-toggler")) {
		toggler.addEventListener("click", toggleDarkmode);
	}
});
