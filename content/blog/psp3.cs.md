---
title: "Páteční Sled Přednášek III (16. 10. 2020)"
date: 2020-10-14T20:27:00+02:00
draft: false
authors: [ "Kyslík" ]
tags: [ "psp" ]
---

{{< button href="https://meet.jit.si/P%C3%A1tek" text="Připojit se k přednášce" >}}
<br>

[Livestream na YouTube](https://youtu.be/rEPahyUnng4)

Nazdar Pátečníci,

opět přecházíme na online formu Pátků a s ní opět přicházejí Páteční Sledy Přednášek. Tentokrát bude probíhat opravdu v **pátek** a to **16. 10. 2020**. Samotné přednášky budou opět probíhat na [Jitsi](https://meet.jit.si/Pátek) a budeme je streamovat na [YouTube](https://youtu.be/rEPahyUnng4), kde bude následně i záznam.

Co však není obvyklé je, že dva přednášející, konkrétně **Mike** a **Nixi**, jsou hosté z [microlabu](https://microlab.space/), což je podobný spolek Pátku na [PedF UK](https://pedf.cuni.cz/PEDF-1.html). Tímto jim moc děkujeme.

Nyní však již k programu:

15:00 - **Vojta Káně** - Náhled pod pokličku patek.cz

15:30 - **Mike** - lfs-musl-clang

16:00 - **Nixi** - >install gentoo (pokud stihne dorazit)

Pokud jste tak ještě neučili a chcete, máte možnost se přihlásit k odběru novinek o těchto akcích [zde](https://forms.gle/SJpUP9XvicqN8ZBo6).
